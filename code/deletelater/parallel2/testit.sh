echo this is a test.

echo checking to see if the module is loaded.
lsmod | grep pport
if [[ $? == "0" ]] ; then
	echo pport module is loaded.
	echo GOOD
else
	echo pport module is not loaded.
	echo BAD;
	exit 1;
fi


echo checking to see if the device file exists.
if [[ -c /dev/pport ]] ; then
	echo /dev/pport exists
	echo GOOD
else
	echo /dev/pport does not exist;
	echo BAD;
	exit 1;
fi

echo checking to see if permissions are ok.
if [[ -w /dev/pport ]] ; then
	echo /dev/pport is not writable
	echo GOOD
else
	echo /dev/pport is not writable;
	echo BAD;
	exit 1;
fi


echo can we turn off all the leds?
echo -n 0 > /dev/pport
if [[ $? == "0" ]] ; then
	echo The code thinks the write was ok.
	echo GOOD
else
	echo The code thinks the write failed.
	echo BAD;
	exit 1;
fi

echo can we turn on all the leds with hex?
echo -n 0xFf > /dev/pport
# TODO add code to driver to read back the last value written
# then you could check your results.
if [[ $? == "0" ]] ; then
	echo The code thinks the write was ok.
	echo GOOD
else
	echo The code thinks the write failed.
	echo BAD;
	exit 1;
fi

echo can we turn on all the leds with decimal?
echo -n 255 > /dev/pport
if [[ $? == "0" ]] ; then
	echo The code thinks the write was ok.
	echo GOOD
else
	echo The code thinks the write failed.
	echo BAD;
	exit 1;
fi

