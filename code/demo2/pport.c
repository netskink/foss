#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
//#include <linux/config.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/ioport.h>
#include <linux/fcntl.h>
#include <asm/io.h>
#include <asm/system.h>
#include <asm/uaccess.h>


MODULE_AUTHOR("John F. Davis");
MODULE_DESCRIPTION("PC-style parallel port driver\
derived from Rubini's Linux Device Driver's book and \
derived from Xavier Calbet's \
article http://www.freesoftwaremagazine.com/articles/drivers_linux");
MODULE_LICENSE("GPL");


#define JFD_DEBUG

/* Declaration of pport.c functions */
int pport_open(struct inode *inode, struct file *filp);
int pport_release(struct inode *inode, struct file *filp);
ssize_t pport_read(struct file *filp, char *buf, size_t count, loff_t *f_pos);
ssize_t pport_write(struct file *filp, char *buf, size_t count, loff_t *f_pos);
void pport_exit(void);
int pport_init(void);
void decode_dec(int digits);
void decode_hex(int digits);

/* structure that declares the usual file */
/* access functions */
struct file_operations pport_fops = {
	read: pport_read,
	write: pport_write,
	open: pport_open,
	release: pport_release
};


/* Global variables of the driver */
/* Major number */
int pport_major = 61;
/* Buffer to store data */
char *pport_buffer;
/* control variable for memory reservatio of the parallel port */
int port;

int pport_init(void) {
	int result;

	/* Registering device */
	result = register_chrdev(pport_major, "pport", &pport_fops);
	if (result < 0) {
		printk("<1> pport: cannot obtain major number %d\n",pport_major);
		return result;
	}

	/* Registering port */
	port = check_region(0x378, 1);
	if (port) {
		printk("<1> pport: cannot reserve %x\n",0x378);
		result = port;
		goto fail;
	}
	request_region(0x378,1,"pport");

	/* Allocating pport for the buffer */
	pport_buffer = kmalloc(10, GFP_KERNEL);
	if (!pport_buffer) {
		result = -ENOMEM;
		goto fail;
	}
	memset(pport_buffer,0,10);

	printk("<1> Inserting pport module\n");
	return 0;

	fail:
		pport_exit();
		return result;
}

void pport_exit(void) {
	/* Freeing the major number */
	unregister_chrdev(pport_major, "pport");

	/* Freeing port */
	if (!port) {
		release_region(0x378,1);
	}

	/* Freeing buffer memory */
	if (pport_buffer) {
		kfree(pport_buffer);
	}

	printk("<1> Removing pport module\n");
}

int pport_open(struct inode *inode, struct file *filp) {
	/* Success */
	return 0;
}

int pport_release(struct inode *inode, struct file *filp) {
	/* Success */
	return 0;
}

ssize_t pport_read(struct file *filp, char *buf, size_t count, loff_t *f_pos) {

	int result;

	/* Transfering data to user space */
	result = copy_to_user(buf,pport_buffer,10);

	/* TODO why not use result? */


		return result;
}

void decode_hex(int digits) {
	int i;
	unsigned char value, intermediate;
	unsigned char mults[4] = {0,1,16,32};

	if (digits < 1)
		return;

	i = value = 0;
	while(digits != 0) {
		if (pport_buffer[i+2] <= '9') {
			intermediate = pport_buffer[i+2] - '0';	
		} else if (pport_buffer[i+2] <= 'F') {
			intermediate = pport_buffer[i+2] - 'A' + 10;	
		} else {
			intermediate = pport_buffer[i+2] - 'a' + 10;	
		}
		value = value + (intermediate)*(mults[digits]);
		digits--;
		i++;
	}
		
	printk("do write %x\n",value);
	outb(value, 0x378);
	wmb();
}

void decode_dec(int digits) {
	int i;
	unsigned char value;
	unsigned char mults[4] = {0,1,10,100};

	if (digits < 1)
		return;

	#ifdef JFD_DEBUG
	printk("digits = %d\n",digits);
	#endif
	i = value = 0;
	while(digits != 0) {
		value = value + (pport_buffer[i] - '0')*(mults[digits]);
	#ifdef JFD_DEBUG
		printk("do write %d\n",value);
	#endif
		digits--;
		i++;
	}
		
	outb(value, 0x378);
	wmb();
}

ssize_t pport_write(struct file *filp, char *buf, size_t count, loff_t *f_pos) {

	int result,i;

	if (count > 10) {
		count = 10;
	}




	result = copy_from_user(pport_buffer,buf,count);
	/* TODO why not use result? */
	*f_pos += count;
	#ifdef JFD_DEBUG
	for (i=0;i<count;i++)
		printk("write pport_buffer[%d] = %x\n",i, pport_buffer[i]);
	#endif

	if (pport_buffer[0] == '0' && pport_buffer[1] == 'x') 
		decode_hex(count-2);
	else
		decode_dec(count);

	return count;

}


/* Declaration of the init and exit functions */
module_init(pport_init);
module_exit(pport_exit);

