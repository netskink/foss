#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
//#include <linux/config.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/ioport.h>
#include <linux/fcntl.h>
#include <asm/io.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include "common.h"


MODULE_AUTHOR("John F. Davis");
MODULE_DESCRIPTION("PC-style parallel port driver\
derived from Rubini's Linux Device Driver's book and \
derived from Xavier Calbet's \
article http://www.freesoftwaremagazine.com/articles/drivers_linux");
MODULE_LICENSE("GPL");


#define JFD_DEBUG

/* Declaration of pport.c functions */
int pport_open(struct inode *inode, struct file *filp);
int pport_release(struct inode *inode, struct file *filp);
ssize_t pport_read(struct file *filp, char *buf, size_t count, loff_t *f_pos);
ssize_t pport_write(struct file *filp, char *buf, size_t count, loff_t *f_pos);
void pport_exit(void);
int pport_init(void);
void decode_dec(int digits);
void decode_hex(int digits);

/* structure that declares the usual file */
/* access functions */
struct file_operations pport_fops = {
	read: pport_read,
	write: pport_write,
	open: pport_open,
	release: pport_release
};


/* Global variables of the driver */
/* Major number */
int pport_major = 61;
/* Buffer to store data */
mybuff_t *pport_buffer;

int pport_init(void) {
	int result;

	/* Registering device */
	result = register_chrdev(pport_major, "pport", &pport_fops);
	if (result < 0) {
		printk("<1> pport: cannot obtain major number %d\n",pport_major);
		return result;
	}


	/* Allocating pport for the buffer */
	pport_buffer = kmalloc(sizeof(mybuff_t), GFP_KERNEL);
	if (!pport_buffer) {
		result = -ENOMEM;
		goto fail;
	}
	memset(pport_buffer,0,sizeof(mybuff_t));

	printk("<1> Inserting pport module\n");
	return 0;

	fail:
		pport_exit();
		return result;
}

void pport_exit(void) {
	/* Freeing the major number */
	unregister_chrdev(pport_major, "pport");

	/* Freeing buffer memory */
	if (pport_buffer) {
		kfree(pport_buffer);
	}

	printk("<1> Removing pport module\n");
}

int pport_open(struct inode *inode, struct file *filp) {
	/* Success */
	printk("pport_open\n");
	return 0;
}

int pport_release(struct inode *inode, struct file *filp) {
	/* Success */
	printk("pport_close\n");
	return 0;
}

ssize_t pport_read(struct file *filp, char *buf, size_t count, loff_t *f_pos) {

	int result;

	/* Transfering data to user space */
	result = copy_to_user(buf,pport_buffer,sizeof(mybuff_t));

	/* TODO check result? */


		return result;
}


ssize_t pport_write(struct file *filp, char *buf, size_t count, loff_t *f_pos) {


	if (count > 10) {
		count = 10;
	}




	copy_from_user(pport_buffer,buf,count);
	/* TODO check result? */
	*f_pos += count;

//	printk("addr = %lx \n", pport_buffer->address);
//	printk("data = %x \n", pport_buffer->data);

	outb(pport_buffer->data, pport_buffer->address);

	return count;

}


/* Declaration of the init and exit functions */
module_init(pport_init);
module_exit(pport_exit);

