#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "common.h"
#include "audio_scale.h"


int write2_driver(int iFile, unsigned long addr, unsigned long data) {

	printf("write2_driver\n");

	
	mybuff_t	myBuff;
	myBuff.address = addr;
	myBuff.data = data;

	write(iFile,&myBuff,sizeof(mybuff_t));


	return 0;	/* everything ok */
}

int test1(int iFile) {
	
	int iRC = -1;
	int i;
	unsigned char read_val;
	unsigned char pCountDownVals[] = {
		C_LO_COUNTDOWN_VAL,
		C_HI_COUNTDOWN_VAL,
		D_LO_COUNTDOWN_VAL,
		D_HI_COUNTDOWN_VAL,
		G_LO_COUNTDOWN_VAL,
		G_HI_COUNTDOWN_VAL
	};


	printf("test1\n");

	for (i=0;i<6;i+=2) {
		iRC = write2_driver(iFile, SYS_TIMER_2_CMD_ADDR, SYS_TIMER_LOAD_NEW_COUNTDOWN_VAL);
		if (0 != iRC) return iRC;
		iRC = write2_driver(iFile, SYS_TIMER_2_COUNTDOWN_ADDR, pCountDownVals[i]);
		if (0 != iRC) return iRC;
		iRC = write2_driver(iFile, SYS_TIMER_2_COUNTDOWN_ADDR, pCountDownVals[i+1]);
		if (0 != iRC) return iRC;
//		iRC = read_io_byte(pFile, SPEAKER_ADDR, &read_val);
//		if (0 != iRC) return iRC;
		iRC = write2_driver(iFile, SPEAKER_ADDR, read_val | SPEAKER_PLAY_VAL);
		if (0 != iRC) return iRC;

		/* play for 1 second */
		sleep(UONE_SEC);
		iRC = write2_driver(iFile, SPEAKER_ADDR, read_val & SPEAKER_STOP_VAL);
		if (0 != iRC) return iRC;
//		iRC = read_io_byte(pFile, SPEAKER_ADDR, &read_val);
//		if (0 != iRC) return iRC;
	}

	return iRC;
}


int main(void) {

    // The following is returned by IOCTL.  It is true if the write succeeds.
 //   BOOL    IoctlResult;

    // The following parameters are used in the IOCTL call
    int              	iFile;        // Handle to device, obtain from CreateFile
    long                IoctlCode;
    unsigned long               DataValue;
    unsigned long               DataLength;
    unsigned long               address;
    unsigned long               ReturnedLength; // Number of bytes returned in output buffer
	int 				iRC = -1;

//	GENPORT_IOCTL_BUFFER genportIoctlBuffer; // Buffer for in/out to driver.





	// Open the Device "file" 
	iFile = open("/dev/pport",O_RDWR);


    // Was the device opened?
    if (iFile == -1 ) {
		printf("Unable to open the device.\n");
		exit(1);
    } else {
		printf("open the device  worked.\n");
	}

	printf("test 1 "); 
	iRC = test1(iFile);
	(0 == iRC) ? printf("passed.\n") : printf("failed.\n");


    if (0 != close(iFile))  {                // Close the Device "file".
    
        printf("Failed to close device.\n");
    }

    exit(0);
}


